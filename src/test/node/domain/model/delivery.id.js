//    Copyright 2018 Quirino Brizi [quirino.brizi@gmail.com]
// 
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
// 
//        http://www.apache.org/licenses/LICENSE-2.0
// 
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.

/* global it */
/* global require */
/* global describe */

const assert = require('assert');
const DeliveryId = require('../../../../main/node/domain/model/delivery.id');

describe('DeliveryId', () => {
  it('creates a new delivery id', () => {
    let actual = DeliveryId.create('abcd');
    // assert
    assert.equal(actual.id, 'abcd');
  });

  it('throws error if id is not provided', () => {
    try {
      DeliveryId.create();
      assert.ok(false)
    } catch (error) {
      assert.ok(true);
    }

  });
});