/* global module */
/* global require */

'use strict'

const Component = require('dino-core').Component;
const Delivery = require('../../domain/model/delivery');

class DeliveryModelTranslator extends Component {

  constructor({ deliveryIdGenerator }) {
    super();

    this.deliveryIdGenerator = deliveryIdGenerator;
  }

  /**
   * Translate the campaign information retrieved from the request to a model object
   * @param {CampaignMessage} message the campaign information 
   */
  translate(message) {
    let id = this.deliveryIdGenerator.generate();
    return Delivery.create(id, message.businessPartner, message.quantity, message.date);
  }
}

module.exports = DeliveryModelTranslator;