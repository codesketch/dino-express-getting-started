/* global module */
/* global require */

'use strict'

const Component = require('dino-core').Component;

class DeliveryService extends Component {

  constructor({ deliveryRepository }) {
    super();
    this.deliveryRepository = deliveryRepository;
  }

  async search() {
    return await this.deliveryRepository.getAll();
  }

  /**
   * Execute the business orchestration to record a new delivery.
   * 
   * @param {Delivery} delivery the delivery information
   * @returns {Delivery} the newly created Delivery
   */
  async record(delivery) {
    return await this.deliveryRepository.add(delivery);
  }
}

module.exports = DeliveryService;