/* global module */
/* global require */

'use strict'

/**
 * A.R. abstraction of a delivery.
 */
class Delivery {

  /**
   * Create a new delivery object
   * @param {DeliveryId} id the id of the delivery
   * @param {string} businessPartner the delivery business partner
   * @param {number} quntity the delivery quantity
   * @param {number} date the delivery timestamp
   */
  constructor(id, businessPartner, quantity, date) {
    this.id = id;
    this.businessPartner = businessPartner;
    this.quantity = quantity;
    this.date = date;
  }

  idAsString() {
    return this.id.id;
  }

  /**
   * Create a new delivery object
   * @param {DeliveryId} id the id of the delivery
   * @param {string} businessPartner the delivery business partner
   * @param {number} quntity the delivery quantity
   * @param {number} date the delivery timestamp
   * @throws {Error} if the id or profile parameter is not provided
   */
  static create(id, businessPartner, quantity, date) {
    if (!id) {
      throw new Error("Delivery identifier must be provided");
    }
    if (!businessPartner) {
      throw new Error("Delivery business partner must be provided");
    }
    if (!quantity) {
      throw new Error("Delivery quantity must be provided");
    }
    if (!date) {
      throw new Error("Delivery date must be provided");
    }
    return new Delivery(id, businessPartner, quantity, date);
  }


}

module.exports = Delivery;