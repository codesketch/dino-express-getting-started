/* global module */
/* global require */

'use strict'

const Component = require('dino-core').Component;

class DeliveryMessageTranslator extends Component {

  /**
   * Translate the delivery model information to a message to be sent to the user
   * @param {Delivery} model the delivery model information 
   */
  translate(model) {
    return {
      id: model.idAsString(),
      businessPartner: model.businessPartner,
      quantity: model.quantity,
      date: model.date
    }
  }
}

module.exports = DeliveryMessageTranslator;