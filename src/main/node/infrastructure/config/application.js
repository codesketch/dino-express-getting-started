/* global module */
/* global require */


'use strict'

const WebConfiguration = require('dino-express').WebConfiguration;


class AppConfiguration extends WebConfiguration {
  configure() {
    return this.asComponentDescriptor(require('dino-express').RoutingConfigurer);
  }
}

module.exports = AppConfiguration;