/* global module */
/* global require */

'use strict'

const { Repository } = require('dino-core');

class DeliveryRepository extends Repository {

  constructor() {
    super();
    this.store = new Map()
  }

  async getAll() {
    return Array.from(this.store.values());
  }

  async add(delivery) {
    this.store.set(delivery.idAsString(), delivery);
    return delivery;
  }
}

module.exports = DeliveryRepository;