'use strict'

const Dino = require('dino-core');

if (!process.env.DINO_CONFIG_PATH) {
  // set default configuration file if not already set.
  process.env.DINO_CONFIG_PATH = 'src/main/resources/config.json';
}
process.env.DINO_CONTEXT_PATH = 'src/main/node';

Dino.run();
