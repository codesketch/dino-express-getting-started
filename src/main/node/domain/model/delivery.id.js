/* global module */
/* global require */

'use strict'

/**
 * V.O. the delivery identifier.
 */
class DeliveryId {

  /**
   * Create a new DeliveryId object
   * @param {String} id the id of the delivery
   */
  constructor(id) {
    this.id = id
  }

  /**
   * Facotory method create a new DeliveryId object.
   * @param {String} id the id of the Delivery
   * @throws {Error} if the id parameter is not provided
   */
  static create(id) {
    if (!id) {
      throw new Error("Delivery identifier must be provided");
    }
    return new DeliveryId(id);
  }


}

module.exports = DeliveryId;