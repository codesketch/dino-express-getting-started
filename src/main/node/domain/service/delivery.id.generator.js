/* global module */
/* global require */

'use strict'

const uuid = require('uuid-v4');

const Component = require('dino-core').Component;
const DeliveryId = require('../model/delivery.id');

class DeliveryIdGenerator extends Component {

  /**
   * Generate a new DeliveryId
   * @returns {UserId} the generated UserId
   */
  generate() {
    return DeliveryId.create(uuid())
  }
}

module.exports = DeliveryIdGenerator;