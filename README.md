# dino-express-getting-started

A sample application to get you started with dino-express

## Start the application
To start the application type `npm start`, after this navigate to http://localhost:3030/api-docs to load the Swagger API user interface.

## Test the application
To start the application type `npm test`