/* global module */
/* global require */

'use strict'

const {Logger} = require('dino-core');
const {Interface, Response} = require('dino-express');


class DeliveryInterface extends Interface {

  constructor({ deliveryModelTranslator, deliveryMessageTranslator, deliveryService }) {
    super()

    this.deliveryModelTranslator = deliveryModelTranslator;
    this.deliveryMessageTranslator = deliveryMessageTranslator;

    this.deliveryService = deliveryService;
  }

  async searchDeliveries(req) {
    let deliveries = await this.deliveryService.search();
    let messages = (deliveries || []).map(delivery => this.deliveryMessageTranslator.translate(delivery))
    return Response.builder(200, messages).setContentType('application/json');
  }

  async recordDelivery(req) {
    let delivery = this.deliveryModelTranslator.translate(req.body);
    let message = this.deliveryMessageTranslator.translate(await this.deliveryService.record(delivery));
    Logger.info(`delivery recorded ${delivery.idAsString()}`);
    return Response.builder(201, message).setContentType('application/json');
  }

}

module.exports = DeliveryInterface;