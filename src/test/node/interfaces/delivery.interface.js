//    Copyright 2018 Quirino Brizi [quirino.brizi@gmail.com]
// 
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
// 
//        http://www.apache.org/licenses/LICENSE-2.0
// 
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.

/* global it */
/* global require */
/* global describe */

const Dino = require('dino-core');
const assert = require('assert');
const request = require('supertest');

describe('Delivery Interface', () => {

  const applicationContext = Dino.run();
  const api = request('http://localhost:3030');

  after(function () { applicationContext.destroy(); });

  it('creates a new delivery', (done) => {
    api
      .post('/deliveries')
      .send({ businessPartner: 'businessPartner', quantity: 100, date: Date.now() })
      .set('Content-Type', 'application/json')
      .expect('Content-Type', /json/)
      .expect(201)
      .then(res => {
        // '{"id":"a43b592c-5edb-4b3c-8133-5456869db90c","businessPartner":"businessPartner","quantity":100,"date":1555815672403}'
        assert.ok(!!res.body.id);
        assert.ok(!!res.body.date);
        assert.equal(res.body.businessPartner, 'businessPartner');
        assert.equal(res.body.quantity, 100);
        done();
      });
  });

  it('finds a delivery', (done) => {
    api
      .get('/deliveries')
      .expect('Content-Type', /json/)
      .expect(200)
      .then(res => {
        assert.equal(res.body.length, 1);
        assert.ok(!!res.body[0].id);
        assert.ok(!!res.body[0].date);
        assert.equal(res.body[0].businessPartner, 'businessPartner');
        assert.equal(res.body[0].quantity, 100);
        done();
      });
  })
});